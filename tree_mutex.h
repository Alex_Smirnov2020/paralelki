#include<iostream>
#include<atomic>
#include<vector>
#include<thread>
#include<cmath>
#include<array>

class peterson_mutex {
public:
	peterson_mutex() {
		want[0].store(false);
		want[1].store(false);
		victim.store(0);
	}

	void lock(int thread_ind) {
		want[thread_ind].store(true);
		victim.store(thread_ind);

		while (want[1 - thread_ind].load() && victim.load() == thread_ind) {
			std::this_thread::yield();
		}
	}

	void unlock(int thread_ind) {
		want[thread_ind].store(false);
	}

private:
	std::atomic<int> victim;
	std::array<std::atomic<bool>, 2> want;
};

class tree_mutex {
public:
	tree_mutex(std::size_t num_threads) :tree_peterson_mutex(pow(2, trunc(log2(num_threads)) + 1)) {
		this->num_threads.store(num_threads);
	}

	void lock(std::size_t thread_index) {
		std::atomic<std::size_t> index_vertex(thread_index + num_threads);
		while (index_vertex.load() > 0) {
			tree_peterson_mutex[index_vertex.load() / 2].lock(index_vertex.load() % 2);
			index_vertex.store(index_vertex.load() / 2);
		}
	}

	void unlock(std::size_t thread_index) {
		thread_index += num_threads;
		std::atomic<int> height(trunc(log2(num_threads.load())) + 1);
		std::vector<std::atomic<size_t> > sons(height);
		std::vector<std::atomic<size_t> > way(height);
		for (int j = 0; j < height; j++) {
			sons[j].store(thread_index % 2);
			thread_index /= 2;
			way[j].store(thread_index);
		}

		for (int j = 0; j < height; j++) {
			tree_peterson_mutex[way.back().load()].unlock(sons.back().load());
			way.pop_back();
			sons.pop_back();
		}
	}

private:
	std::vector<peterson_mutex> tree_peterson_mutex;
	std::atomic<std::size_t> num_threads;
};