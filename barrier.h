#include<iostream>
#include<atomic>
#include<thread>

class barrier {
public:
	explicit barrier(size_t num_threads);
	void enter();
private:
	size_t number_threads;
	std::atomic<size_t> space;
	std::atomic<size_t> era;
};

barrier::barrier(size_t num_threads): number_threads(num_threads), space(0), era(0) {}

void barrier::enter() {
	size_t thread_era = era;
	space++;
	if (space == number_threads) {
		space = 0;
		era++;
	}
	else {
		while (era == thread_era) {
			std::this_thread::yield();
		}
	}
}