//�������, 492 ������
#include<iostream>
#include<mutex>
#include<thread>
#include<queue>
#include<vector>
#include <future>
#include<condition_variable>

template <class Value>
class thread_safe_queue {
private:
	std::mutex mut;
	std::queue<Value> queue;
	std::condition_variable empty_cv;
	std::condition_variable full_cv;
	std::size_t capacity;
	bool shutdown_flag;

public:
	explicit thread_safe_queue(std::size_t _capacity = 100);
	void enqueue(Value& v);
	void pop(Value& v);
	void shutdown();
	thread_safe_queue(const thread_safe_queue&) = delete;
	thread_safe_queue operator = (const thread_safe_queue&) = delete;
};

template <class Value>
thread_safe_queue<Value>::thread_safe_queue(std::size_t _capacity) {
	this->capacity = _capacity;
	shutdown_flag = 1;
}

template <class Value>
void thread_safe_queue<Value>::shutdown() {
	std::unique_lock<std::mutex> lock(this->mut);
	this->shutdown_flag = 0;
	this->empty_cv.notify_all();
	this->full_cv.notify_all();
}

template <class Value>
void thread_safe_queue<Value>::enqueue(Value& v) {
	std::unique_lock<std::mutex> lock(this->mut);
	this->full_cv.wait(lock, [this]() {
		if (!this->shutdown_flag)
			throw std::exception();
		return !(this->queue.size() >= this->capacity); });
	this->queue.push(move(v));

	this->empty_cv.notify_one();
}

template <class Value>
void thread_safe_queue<Value>::pop(Value& v) {
	std::unique_lock<std::mutex> lock(this->mut);
	this->empty_cv.wait(lock, [this]() {
		if (!this->shutdown_flag && queue.empty())
			throw std::exception();
		return !(this->queue.empty()); });
	v = move(this->queue.front());
	this->queue.pop();

	full_cv.notify_one();
}




template <class Value>
class thread_pool {
public:
	explicit thread_pool(size_t num_workers = default_num_workers());
	std::future<Value> submit(std::function<Value()> function);
	void shutdown();
	static size_t default_num_workers();

private:
	std::vector<std::thread> workers;
	thread_safe_queue<std::packaged_task<Value()>> queue;
};

template <class Value>
size_t thread_pool<Value>::default_num_workers() {
	size_t num_workers = std::thread::hardware_concurrency();
	if (num_workers)
		return num_workers;
	else
		return 2;
}

template <class Value>
void thread_pool<Value>::shutdown() {
	queue.shutdown();
	for (size_t i = 0; i < workers.size(); i++) {
		workers[i].join();
	}
}

template <class Value>
std::future<Value> thread_pool<Value>::submit(std::function<Value()> function) {
	std::packaged_task<Value()> task(function);
	std::future<Value> result = task.get_future();
	queue.enqueue(task);
	return result;
}

template <class Value>
thread_pool<Value>::thread_pool(size_t num_workers) {
	workers.resize(num_workers);

	for (size_t i = 0; i < num_workers; i++) {
		workers[i] = std::thread([this]() {
			std::packaged_task<Value()> it;
			try {
				while (1) {
					queue.pop(it);
					it();
			}
			}
			catch (const std::exception&) {
				return;
			}
		}
		);
	}
}
