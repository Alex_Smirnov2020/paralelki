#include<atomic>
#include<vector>

template <class Value>
class spsc_ring_buffer {
public:
	explicit spsc_ring_buffer(size_t _size): data(_size + 1), size(_size+1) {}
	bool enqueue(Value v);
	bool dequeue(Value& v);
private:
	std::vector<Value> data;
	std::atomic<size_t> head{0}, tail{0};
	const size_t size;
};

template <class Value>
bool spsc_ring_buffer<Value>::enqueue(Value v) {
	size_t curr_tail = tail.load(std::memory_order_relaxed);		//��� �������, ��� ����� ���� ������: ���������� ������ tail ��� ������ head
	size_t curr_head = head.load(std::memory_order_acquire);		//����������, ����� ������� �� ���������� ����������, � ����� ����������� ���������, 
	size_t next_tail = (curr_tail + 1) % size;						//����� �� ���� ���������������� ������ � dequeue. ��� �� �����,
	if (next_tail == curr_head)										
		return false;												//����� �������� ������������� ����� ������� ����������� �� ����, ��� �� �������� ���� ������� 
	data[curr_tail] = v;											//� ��������� �������� ������.
	tail.store(next_tail, std::memory_order_release);		//��, ��� ����� acquire � release ��������� ����� ���� � �� ������ �� ������� ����� "�������".
	return true;
}

template <class Value>
bool spsc_ring_buffer<Value>::dequeue(Value& v) {
//	size_t curr_tail = tail.load(std::memory_order_relaxed);		//���������� � �������� enqueue()
	size_t curr_head = head.load(std::memory_order_relaxed);
	if (tail.load(std::memory_order_acquire) == curr_head)
		return false;
	v = data[curr_head];
	size_t tmp = (curr_head + 1) % size;
	head.store(tmp, std::memory_order_release);
	return true;
}