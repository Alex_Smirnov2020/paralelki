//�������, 492 ������
#include<iostream>
#include<mutex>
#include<thread>
#include<queue>
#include<condition_variable>

template <class Value, class Container = std::deque<Value>>
class thread_safe_queue {
private:
	std::mutex mut;
	Container queue;
	std::condition_variable empty_cv;
	std::condition_variable full_cv;
	std::size_t capacity;
	bool shutdown_flag;

public:
	explicit thread_safe_queue(std::size_t _capacity);
	void enqueue(Value v);
	void pop(Value& v);
	void shutdown();
	thread_safe_queue(const thread_safe_queue&) = delete;
	thread_safe_queue operator = (const thread_safe_queue&) = delete;
};

template <class Value, class Container>
thread_safe_queue<Value, Container>::thread_safe_queue(std::size_t _capacity) {
	this->capacity = _capacity;
	shutdown_flag = 1;
}

template <class Value, class Container>
void thread_safe_queue<Value, Container>::shutdown() {
	std::unique_lock<std::mutex> lock(this->mut);
	this->shutdown_flag = 0;
	this->empty_cv.notify_all();
	this->full_cv.notify_all();
}

template <class Value, class Container>
void thread_safe_queue<Value, Container>::enqueue(Value v) {
	std::unique_lock<std::mutex> lock(this->mut);
		this->full_cv.wait(lock, [this]() {
			if (!this->shutdown_flag)
				throw std::exception();
			return !(this->queue.size() >= this->capacity); });
		this->queue.push_back(move(v));

		this->empty_cv.notify_one();
}

template <class Value, class Container>
void thread_safe_queue<Value, Container>::pop(Value& v) {
	std::unique_lock<std::mutex> lock(this->mut);
	this->empty_cv.wait(lock, [this]() {
			if (!this->shutdown_flag && queue.empty())
				throw std::exception();
			return !(this->queue.empty()); });
		v = move(this->queue.front());
		this->queue.pop_front();

		full_cv.notify_one();
}
