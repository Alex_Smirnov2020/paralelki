#include<forward_list>
#include<shared_mutex>
#include<iostream>
#include<vector>

template <class Value, class Hash = std::hash<Value> >
class striped_hash_set {
public:
	explicit striped_hash_set(size_t n_stripes, float _load_factor = 0.5, size_t _growth_factor = 3);
	void add(const Value& v);
	void remove(const Value& v);
	bool contains(const Value& v);

private:
	std::atomic<size_t> num_el;
	size_t growth_factor;		//����������������� ����������� ����� ���-�������
	float load_factor;			//��������� ����� ��������� � ����� ������
	std::atomic<size_t> size;
	Hash hash;
	mutable std::deque<std::shared_timed_mutex> locks;
	std::vector<std::forward_list<std::pair<Value, int> > > data;
	size_t get_bucket_index(size_t hash_value) {
		return hash_value % size;
	}
	size_t get_stripe_index(size_t hash_value) {
		return hash_value % locks.size();
	}
	void resize();
};

template <class Value, class Hash>
void striped_hash_set<Value, Hash>::resize() {
	size.store(size.load() * growth_factor);
	std::vector<std::forward_list<std::pair<Value, int> > > new_data(size.load() * growth_factor);
	for (auto bucket : data) {
		for (auto val : bucket) {
			new_data[get_bucket_index(val.second)].push_front(val);
		}
	}
	data.swap(new_data);
}

template <class Value, class Hash>
striped_hash_set<Value, Hash>::striped_hash_set(size_t n_stripes, float _load_factor, size_t _growth_factor) :
	num_el(0),
	growth_factor(_growth_factor),
	load_factor(_load_factor),
	size(n_stripes * 5),
	locks(n_stripes),
	data(n_stripes * 5) {}


template <class Value, class Hash>
void striped_hash_set<Value, Hash>::add(const Value& v) {
	auto h = hash(v);
	size_t size_of_data;
	if (contains(v))
		return;
	auto index_of_stripe = get_stripe_index(h);
	{
	std::unique_lock<std::shared_timed_mutex> lk(locks[index_of_stripe]);
	auto index_of_bucket = get_bucket_index(h);
	data[index_of_bucket].push_front(std::make_pair(v, h));
	num_el.fetch_add(1);
	size_of_data = size.load();
	}
	if (num_el.load() / size_of_data >= load_factor) {
		std::vector<std::unique_lock<std::shared_timed_mutex> > mut;
		mut.emplace_back(locks[0]);
		if (size.load() != size_of_data)
			return;
		for (size_t i = 1; i < locks.size(); i++) {
			mut.emplace_back(locks[i]);
		}
		resize();
	}
}

template <class Value, class Hash>
void striped_hash_set<Value, Hash>::remove(const Value& v) {
	size_t hash_val = hash(v);
	size_t index_of_stripe = get_stripe_index(hash_val);
	std::unique_lock<std::shared_timed_mutex> mut(locks[index_of_stripe]);
	size_t index_of_bucket = get_bucket_index(hash_val);
	data[index_of_bucket].remove(std::make_pair(v, hash_val));
}

template <class Value, class Hash>
bool striped_hash_set<Value, Hash>::contains(const Value& v) {
	size_t hash_val = hash(v);
	size_t index_of_stripe = get_stripe_index(hash_val);
	std::shared_lock<std::shared_timed_mutex> mut(locks[index_of_stripe]);
	size_t index_of_bucket = get_bucket_index(hash_val);
	for (auto iter : data[index_of_bucket])
		if (iter.first == v)
			return true;
	return false;
}