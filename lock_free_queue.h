#include <vector>
#include <iostream>
#include <memory>
#include <thread>
#include <atomic>

template <class T>
class lock_free_queue{
    	struct node{
        	std::atomic<node*> next;
        	std::unique_ptr<T> data;
        	node () : next(nullptr), data(nullptr) {}
        	node (T& value) : next(nullptr), data(new T{value}) {}
    	};
    	std::atomic<size_t> num_of_workers;
    	std::atomic<node*> tail;
    	std::atomic<node*> head;
    	std::atomic<node*> extracted;
public:
    	lock_free_queue (){
        	node* new_node = new node{};
        	head.store(new_node);
        	tail.store(new_node);
        	extracted.store(new_node);
    	}
    	~lock_free_queue (){
        	for (node* it = extracted.load(); it != nullptr;){
           		node* tmp = it->next.load();
            		delete it;
            		it = tmp;
        	}
    	}
    	void enqueue(T value){
        	node* new_node = new node{value};
        	node* current_tail;
        	num_of_workers.fetch_add(1);
        	while(true){
            		current_tail = tail.load();
            		node* current_tail_next = current_tail->next.load();
            		if (current_tail_next == nullptr){
                		if ((tail.load())->next.compare_exchange_weak(current_tail_next, new_node)) {
                    			break;
				}
            		} else {
                		tail.compare_exchange_strong(current_tail, current_tail_next);
	    		}
        	}
        	tail.compare_exchange_weak(current_tail, new_node);
        	num_of_workers.fetch_sub(1);
    	}
    	bool dequeue(T& value){
        	num_of_workers.fetch_add(1);
        	node* current_head;
        	node* current_tail;
        	node* current_head_next;
        	while(true){
            		current_head = head.load();
            		current_tail = tail.load();
            		current_head_next = current_head->next.load();
            		if (current_head == current_tail){
                		if (!current_head_next){
                    			num_of_workers.fetch_sub(1);
                    			return false;
                		} else {
                    			tail.compare_exchange_weak(current_head, current_head_next);
				}
            		}else if (head.compare_exchange_weak(current_head, current_head_next)) {
                		value = *(current_head_next->data);
                		break;
            		}
        	}
        	if (num_of_workers.load() == 1){
            		node* extracted_node = extracted.exchange(current_head_next);
            		while (extracted_node != extracted.load()){
                		node * tmp = extracted_node->next.load();
                		delete extracted_node;
                		extracted_node = tmp;
            		}
        	}
        	num_of_workers.fetch_sub(1);
        	return true;
    	}
};
